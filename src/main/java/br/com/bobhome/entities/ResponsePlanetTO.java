package br.com.bobhome.entities;

import java.util.List;

public class ResponsePlanetTO {
	
	private List<PlanetTODTO> results;
	
	public List<PlanetTODTO> getResults() {
		return results;
	}
	public void setResults(List<PlanetTODTO> results) {
		this.results = results;
	}
	
}
