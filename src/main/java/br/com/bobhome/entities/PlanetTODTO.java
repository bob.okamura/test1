package br.com.bobhome.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.List;

public class PlanetTODTO {

	private String name;
	private String diameter;
	private String climate;
	private String gravity;
	private String terrain;
	private String population;
	private List<String> films;

	public String getName() {
		return name;
	}

	public String getDiameter() {
		return diameter;
	}

	public String getClimate() {
		return climate;
	}

	public String getGravity() {
		return gravity;
	}

	public String getTerrain() {
		return terrain;
	}

	public String getPopulation() {
		return population;
	}

	public List<String> getFilms() {
		return films;
	}
}